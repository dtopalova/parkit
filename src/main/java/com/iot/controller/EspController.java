package com.iot.controller;

import java.util.List;
import java.util.Set;

import com.iot.model.EspDto;
import com.iot.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.iot.exception.EspNotFoundException;
import com.iot.model.Esp;
import com.iot.service.EspService;

@RestController
@RequestMapping("/api/esps")
public class EspController {

	private EspService espService;

	@Autowired
	public EspController(EspService espService) {
		this.espService = espService;
	}

	@GetMapping
	public List<EspDto> allEsps() {
		return espService.findAll();
	}

	@GetMapping("/{id}")
	EspDto one(@PathVariable Long id) throws EspNotFoundException{
		return espService.findById(id);
	}

	@PostMapping
	String newEsp(@RequestBody EspDto newEsp) {
		System.out.println("POST esp:");
		System.out.println(newEsp.toString());
		EspDto createdEsp= espService.create(newEsp);
		return createdEsp.getId().toString();
	}

	@PutMapping("/{id}")
	public void update(@PathVariable String id, @RequestBody EspDto esp) {
		System.out.println("PUT esp:");
		System.out.println(esp.toString());

		Long espId = Long.parseLong(id);
		espService.update(espId, esp);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable String id) {
		Long espId = Long.parseLong(id);
		espService.deleteById(espId);
	}

	@GetMapping("/closest")
	public Location findClosestParking(@RequestParam float lat, @RequestParam float lon) {
		return espService.findClosestFreeParking(lat, lon);
	}

	@GetMapping("/radius")
	public Set<Location> findFreeParkingsInRadius(@RequestParam float lat, @RequestParam float lon, @RequestParam float radius) {
		return espService.findFreeParkingsInRadius(lat, lon, radius);
	}
}
