package com.iot.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.iot.model.EspDto;
import com.iot.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iot.dao.EspRepository;
import com.iot.model.Esp;

@Service
public class EspServiceImpl implements EspService {

	private EspRepository espRepository;

	@Autowired
	public EspServiceImpl(EspRepository espRepository) {
		this.espRepository = espRepository;
	}

	@Override
	public List<EspDto> findAll() {
		Iterable<Esp> it = espRepository.findAll();
		List<EspDto> esps = new ArrayList<>();
		it.forEach(e -> esps.add(e.ToDto()));
		return esps;
	}

	@Override
	public EspDto findById(Long espId) {
		return espRepository.findById(espId).get().ToDto();
	}

	@Override
	public EspDto create(EspDto dto) {
		return espRepository.save(dto.ToEspEntity()).ToDto();
	}

	@Override
	public EspDto update(Long espId, EspDto dto) {
		Esp espToBeUpdated = espRepository.findById(espId).get();
		espToBeUpdated.setMAC(dto.getMac());
		espToBeUpdated.setLocationLongitude(dto.getLocation().getLongitude());
		espToBeUpdated.setLocationLatitude(dto.getLocation().getLatitude());
		espToBeUpdated.setIsFree(dto.getIsFree());
		espRepository.save(espToBeUpdated);

		return espToBeUpdated.ToDto();
	}

	@Override
	public void deleteById(Long espId) {
		espRepository.deleteById(espId);
	}

	@Override
	public void deleteAll() {
		espRepository.deleteAll();
	}

	@Override
	public Location findClosestFreeParking(float latitude, float longitude) {
		Location location = new Location(latitude, longitude);

		List<EspDto> esps = findAllFreeEsps();

		Location closest = esps.get(0).getLocation();
		double minDistance = findDistance(location, closest);

		for (int i = 1; i < esps.size(); i++) {
			Location currentLocation = esps.get(i).getLocation();
			double currentDistance = findDistance(location, currentLocation);
			if (currentDistance < minDistance) {
				minDistance = currentDistance;
				closest = currentLocation;
			}
		}

		return closest;
	}

	@Override
	public Set<Location> findFreeParkingsInRadius(float latitude, float longitude, float radius) {
		Location userLocation = new Location(latitude, longitude);

		Set<Location> resultLocations = new HashSet<>();

		List<EspDto> esps = findAllFreeEsps();

		for (EspDto esp : esps) {
			double currentDistance = findDistance(userLocation, esp.getLocation());
			if (currentDistance <= radius) {
				resultLocations.add(esp.getLocation());
			}
		}

		return resultLocations;
	}

	// Distance between two point calculated with the Haversine formula
	// resource: https://www.movable-type.co.uk/scripts/latlong.html?fbclid=IwAR36tUkye30wUQBoAa3Sey6Lp664CTXZCMZ02pJ03nFHiWgN2qAC6gfvwZA
	private double findDistance(Location from, Location to) {
		int EARTH_MEAN_RADIUS = 6378137; // Earth’s mean radius in meter

		double dLat = Math.toRadians(to.getLatitude() - from.getLatitude());
		double dLong = Math.toRadians(to.getLongitude() - from.getLongitude());
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos(Math.toRadians(from.getLatitude())) * Math.cos(Math.toRadians(to.getLatitude())) *
						Math.sin(dLong / 2) * Math.sin(dLong / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_MEAN_RADIUS * c; // returns the distance in meter
	}

	private List<EspDto> findAllFreeEsps() {
		List<EspDto> freeEsps = findAll().stream()
				.filter(EspDto::getIsFree).collect(Collectors.toList());

		return freeEsps;
	}


}
