package com.iot.service;

import java.util.List;
import java.util.Set;

import com.iot.exception.EspNotFoundException;
import com.iot.model.Esp;
import com.iot.model.EspDto;
import com.iot.model.Location;

public interface EspService {
	List<EspDto> findAll();

	EspDto findById(Long id) throws EspNotFoundException;

	EspDto create(EspDto dto);

	EspDto update(Long espId, EspDto dto);

	void deleteById(Long id);

	void deleteAll();

	Location findClosestFreeParking(float latitude, float longitude);

	Set<Location> findFreeParkingsInRadius(float latitude, float longitude, float radius);

}
