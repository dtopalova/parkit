package com.iot.exception;

public class EspNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7162581094701352945L;

	public EspNotFoundException(String message) {
		super(message);
	}
}
