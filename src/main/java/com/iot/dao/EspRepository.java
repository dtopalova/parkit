package com.iot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iot.model.Esp;

@Repository
public interface EspRepository extends CrudRepository<Esp, Long> {

}
