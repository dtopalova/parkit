package com.iot.model;

public class EspDto {
    private Long id;

    private String mac;

    private Location location;

    private boolean isFree;

    public EspDto() {
    }

    public EspDto(Long id, String mac, Location location, boolean isFree) {
        this.id = id;
        this.mac = mac;
        this.location = location;
        this.isFree = isFree;
    }

    public EspDto(String mac, boolean isFree) {
        this.mac = mac;
        this.isFree = isFree;
    }

    public Long getId() {
        return id;
    }

    public String getMac() {
        return mac;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean getIsFree() {
        return isFree;
    }

    public void setFree(boolean isFree) {
        this.isFree = isFree;
    }

    public Esp ToEspEntity() {
        Esp result = new Esp();
        if (this.id != null) {
            result.setId(this.getId());
        }
        result.setIsFree(this.getIsFree());
        result.setMAC(this.getMac());
        result.setLocationLatitude(this.getLocation().getLatitude());
        result.setLocationLongitude(this.getLocation().getLongitude());

        return result;
    }

    @Override
    public String toString() {
        return "EspDto{" +
                "id=" + id +
                ", MAC='" + mac + '\'' +
                ", location=" + location +
                ", isFree=" + isFree +
                '}';
    }
}
