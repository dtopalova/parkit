package com.iot.model;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

@Entity
@Table(name = "esps")
public class Esp {

	@PositiveOrZero
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "mac")
	private String MAC;

	@Column(name = "location_latitude")
	private float locationLatitude;

	@Column(name = "location_longitude")
	private float locationLongitude;

	@Column(name = "is_free")
	private boolean isFree;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}

	public float getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(float locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	public float getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(float locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	public boolean getIsFree() {
		return this.isFree;
	}
	public void setIsFree(boolean status) {
		this.isFree = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Esp)) return false;
		Esp esp = (Esp) o;
		return Float.compare(esp.getLocationLatitude(), getLocationLatitude()) == 0 &&
				Float.compare(esp.getLocationLongitude(), getLocationLongitude()) == 0 &&
				isFree == esp.isFree &&
				getId().equals(esp.getId()) &&
				getMAC().equals(esp.getMAC());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getMAC(), getLocationLatitude(), getLocationLongitude(), isFree);
	}

	public EspDto ToDto() {
		EspDto result = new EspDto();
		result.setId(this.getId());
		result.setFree(this.getIsFree());
		result.setMac(this.getMAC());
		Location location = new Location(this.getLocationLatitude(), this.getLocationLongitude());
		result.setLocation(location);

		return result;
	}
}
