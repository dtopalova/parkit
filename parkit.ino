// Install from zip - https://playground.arduino.cc/Code/NewPing/
#include <NewPing.h>

// Install from Sketch -> Include Library -> Manage Libraries
#include <ArduinoJson.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>
#include <WiFi.h>


#define MAX_DISTANCE 13 // Distane between car and sensor in sm (min 2-3sm for the sensor to work)

// first sensor data
#define PARKING_SPACE_MAC_ONE "MAC1"
#define TRIGGER_PIN_ONE  2
#define ECHO_PIN_ONE     5

// https://core-electronics.com.au/tutorials/how-to-use-ultrasonic-sensors.html
NewPing sonarOne(TRIGGER_PIN_ONE, ECHO_PIN_ONE, MAX_DISTANCE);
bool isFreeOne;
bool isPostOne;
String latitudeOne = "0.0";
String longitudeOne = "0.0";
String parkingPlaceIdOne;

// second sensor data
#define PARKING_SPACE_MAC_TWO "MAC2"
#define TRIGGER_PIN_TWO  22
#define ECHO_PIN_TWO     23

// https://core-electronics.com.au/tutorials/how-to-use-ultrasonic-sensors.html
NewPing sonarTwo(TRIGGER_PIN_TWO, ECHO_PIN_TWO, MAX_DISTANCE);
bool isFreeTwo;
bool isPostTwo;
String latitudeTwo = "90.0";
String longitudeTwo = "90.0";
String parkingPlaceIdTwo;


String serverBaseUrl = "http://192.168.0.103:8080";
WiFiMulti wiFiMulti;
HTTPClient http;

void setup() {
//  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200L);

  // We start by connecting to a WiFi network
  connectToWIFI();
  
  // Send inital request for free space
  isFreeOne = true;
  isPostOne = true;
  sendStatusOne();
  
  isFreeTwo = true;
  isPostTwo = true;
  sendStatusTwo();
}


void loop() {
  // Parking space just taken
  if (sonarOne.ping_cm() != 0 && isFreeOne){
    isFreeOne = false;
    sendStatusOne();
  }

  // Parkign space just freed
  if (sonarOne.ping_cm() == 0 && !isFreeOne){
    isFreeOne = true;
    sendStatusOne();
  }

  // Parking space just taken
  if (sonarTwo.ping_cm() != 0 && isFreeTwo){
    isFreeTwo = false;
    sendStatusTwo();
  }

  // Parkign space just freed
  if (sonarTwo.ping_cm() == 0 && !isFreeTwo){
    isFreeTwo = true;
    sendStatusTwo();
  }
  delay(300);
}

// File -> examples -> WiFI -> WiFiClientBasic
void connectToWIFI() {
  const char* ssid = "TP-LINK_3A7C";
  const char* password = "65930403";

  wiFiMulti.addAP(ssid, password);
  
  Serial.println("Waiting for WiFi... ");

  while (wiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
}

// https://www.techcoil.com/blog/how-to-post-json-data-to-a-http-server-endpoint-from-your-esp32-development-board-with-arduinojson/
void sendStatusOne()
{
  Serial.print("SEND STATUS ONE:");
  if (wiFiMulti.run() != WL_CONNECTED) {
    connectToWIFI();
  }

  String requestBody = createRequestBody(String(PARKING_SPACE_MAC_ONE), String(isFreeOne), latitudeOne, longitudeOne);
  
  Serial.println("Request body: " + requestBody);

  if(isPostOne){
    sendRequestToServer(serverBaseUrl + "/api/esps", requestBody, "POST");
    isPostOne = false;
    parkingPlaceIdOne = http.getString();
  }
  else{
    sendRequestToServer(serverBaseUrl + "/api/esps/" + parkingPlaceIdOne, requestBody, "PUT");
  }
}


// https://www.techcoil.com/blog/how-to-post-json-data-to-a-http-server-endpoint-from-your-esp32-development-board-with-arduinojson/
void sendStatusTwo()
{
  Serial.print("SEND STATUS TWO:");
  if (wiFiMulti.run() != WL_CONNECTED) {
    connectToWIFI();
  }

  String requestBody = createRequestBody(String(PARKING_SPACE_MAC_TWO), String(isFreeTwo), latitudeTwo, longitudeTwo);

  Serial.println("Request body: " + requestBody);

  if(isPostTwo){
    sendRequestToServer(serverBaseUrl + "/api/esps", requestBody, "POST");
    isPostTwo = false;
    parkingPlaceIdTwo = http.getString();
  }
  else{
    sendRequestToServer(serverBaseUrl + "/api/esps/" + parkingPlaceIdTwo, requestBody, "PUT");
  }

}

String createRequestBody(String parkingPlaceId, String isFree, String latitude, String longitude){
  String location = "\"location\": { \"latitude\": " + latitude + ", \"longitude\": " + longitude + "}";
  return "{\"mac\": \"" + parkingPlaceId + "\", \"isFree\":" + isFree + "," + location + "}";
}

int sendRequestToServer(String serverUrl, String requestBody, String method){
  http.begin(serverUrl);
  http.addHeader("Content-Type", "application/json");
  int resCode;
  if(method == "POST"){
    resCode = http.POST(requestBody);
  }
  else{
    resCode = http.PUT(requestBody);
  }

  if (resCode > 0) {

  Serial.println(resCode);;
  }
  return resCode;
}
